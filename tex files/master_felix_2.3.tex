\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
%\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{makeidx}
\usepackage{float}
\usepackage{graphicx}
\graphicspath{{../../graphics/}}
\usepackage{wrapfig}
\usepackage{acronym}
\usepackage[onehalfspacing]{setspace}
\usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{hyperref}
\setlength{\parindent}{0pt}
\newcommand{\comment}[1]{}


\author{Felix Risse}
\title{Computational models of pattern generation during developmental processes in plants}
\date{2020}


\begin{document}


 
\section{Models}



\subsection{MoleculeX model}
\label{sec:molxmodel}


\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{/molx/molx.png}
\caption{Schematic of the one-dimensional "MoleculeX" model, which is based on the "Apoplast model. Beside auxin, a diffusible molecule ($MX_{i}$) is produced, dependent on the auxin concentration, in a cell and can freely diffuse between cells and apoplasts. Together with the auxin in the apoplasts, it is a factor for the regulation of the PIN re-distribution.}
\end{figure}


The last of the three main models is the "MoleculeX" model.
Again, it is based on the previous "Apoplast" model with the addition of a new component, a freely diffusable signal molecule, named as "moleculeX".
Fujita et al. \cite{Fujita2018} tried to find a method to re-generate a regular pattern from the model with the apoplast and an approach was to introduce a new regulational component to the system.
However, they don't used known molecules and fitted the in the model, but concentrated first on finding the type of regulation, which could lead to the generation of a regular pattern.\\
The best result was a molecule, that passes membranes by simple diffusion, produced in the cytosol dependent on the present abundance of auxin and, like auxin, has an influence on the PIN distribution in neighboring cells.
Known molecules, that may not completely hold all this features, but also affect the PIN polarization and auxin transport, are, for example, strigolactone \cite{shinohara2013}, which triggers the depletion of PIN of the membrane, or GOLVEN \cite{whitford2012}, which has a positive effect on PIN2 localization in the membrane.



\subsubsection{Mathematical equations}
\label{sec:molxequation}

Additionally to the previous mathematical description of the model, two new ODEs has to be added for the concentraion of moleculeX in the cytosol and apoplasts.
Except the contribution to the regulation of the PIN distribution, it does not affect the behavior of the other components at all.

\begin{equation}
\large  \frac{dx_{i}}{dt}=G_{x}(\Theta(a_{i})-x_{i})+\sum_{j} D_{x}(x'_{i,j}-x_{i}) \label{eq:molx_xi}
\end{equation}
\addtocounter{equation}{-1}

\begin{subequations}
\begin{align}
\Large 	\Theta(a_{i})=\frac{2*a_{i}^{r}}{A^{r} + a_{i}^{r}} \label{eq:molx_theta}
\end{align}
\end{subequations}

\begin{equation}
\large  \frac{dx'_{i,j}}{dt}=-G_{x}x'_{i,j}+\frac{D_{x}}{V}\left(x_{i}+x_{j}-2x'_{i,j}\right) \label{eq:molx_xij}
\end{equation}

Since the involvement of the moleculeX is very small and it just is transported by diffusion, the equations have a high resemblance to the auxin ODE.
Inside the cytosol \ref{eq:molx_xi}, it has a term for its synthesis with a synthesis coefficient $G_{x}$ and one for the diffusion to the adjacent apoplasts with a diffusion coefficient $D_{x}$.
Though, instead of a parameter for a base level, it has a regulation function $\Theta()$ \ref{eq:molx_theta} with a new factor $r$.\\
Also, the equations for the apoplast concentraion \ref{eq:molx_xij} consists of the same terms for the degradation and diffusion as auxin, but obviously with the specific synthesis and diffusion for moleculeX.


\subsection{Analysis}
\label{sec:molxanalysis}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{/molx/standard.png}
\caption{Simulation of the "MoleculeX" model under different standard conditions: (A) Standard for the further analysis of the model, (B) standards of the "Apoplast" model and (C) standards of the "Simple" model. Unchanged parameters are kept as they are in the "MoleculeX" standard.}
\label{fig:molx_standard}
\end{figure}

The introduction of the moleculeX re-enable the ability of producing a regular pattern with the use of a new pre-selected parameter set.
Simulating it with the previous parameters results in different patterns, though, the "Simple" standard also produces a regular pattern, while the "Apoplast" standard just creates an irregular pattern \ref{fig:molx_standard}. 
However, this model is based on the "Apoplast" model, which is not capable of generating a comparable pattern like the "Simple" model, but is able to generate a irregular and regular pattern.

 
\begin{table}
\centering
\begin{tabular}{|r|r|r|}
\hline
Variable&Value	&Definition\\
\hline
\multicolumn{3}{|c|}{Conditions}\\
\hline
N			&50		&total cell number\\
K			&2		&number of adjacent cells per cell\\
Auxin		&6		&initial auxin concentration per cell\\
MoleculeX	&5		&inital concentration of moleculeX per cell\\
PIN			&40		&initial PIN concentration per membrane side per cell\\
Time		&100000	&maximal integration time\\
Noise		&.01		&relative noise on initial values\\
PBC			&1		&boundary condition\\
\hline
\multicolumn{3}{|c|}{Parameter}\\
\hline
$G_{a}$	&1		&auxin synthesis coefficient\\
A		&6		&auxin base level\\
$D_{a}$	&35.5	&auxin diffusion coefficient\\
$G_{p}$	&2		&PIN synthesis coefficient\\
P		&40		&PIN base level\\
m		&1		&PIN regulation factor\\
$E_{p}$	&1		&PIN efflux efficiency coefficient\\
$E_{q}$	&1		&AUX influx efficiency coefficient\\
q		&40		&AUX concentration per membrane\\
V		&1		&apoplast-cytosol volume ratio\\
$G_{x}$	&1		&moleculeX synthesis coefficient\\
$D_{x}$	&10		&moleculeX diffusion coefficient\\
r		&4		&moleculeX synthesis factor\\
mx		&3		&moleculeX regulation factor\\
\hline
\end{tabular}
\caption{Values and definitions of the parameters used as the standard for analysis of the "MoleculeX" model.}
\label{tab:molx_standard}
\end{table}


\comment{
\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/molx/.png}
\caption{}
\label{fig:}
\end{figure}
}

\begin{figure}
\centering
\includegraphics[scale=.7]{/molx/Data_cn_90.png}
\caption{Simulations under moleculeX standard conditions with increasing number of cells and apoplasts.}
\label{fig:molx_cn}
\end{figure}

A change of the model size is not influencing the regularity of the pattern and the behavior of the model by increasing the cell number \ref{fig:molx_cn} is similar to the "Simple" model \ref{fig:1dsimple_cn}. 
While the size is increasing, the peak heights and total auxin concentration are rising until a point, where a new peak is generated and the peak concentrations instantly decrease.
This procedure repeats, while the rate, at which the peak height is increasing, is weakening as it approaches a certain value.
An assumption for this behaviour could be, that with the selected model parameter, a regular pattern has a maximal peak height.
By the increase of the space, where auxin could be distributed, peaks grow in width and height as much as they can, but at some point, a new peak is generated and the total auxin is re-distributed to maintain the structure of the pattern. 

Similar to the "Simple" model, the initial values for auxin, PIN or moleculeX and the applied noise have no observable effect on the model behavior and even the relation between the initial auxin concentration and the base level $A$ \ref{fig:1dsimple_iva_aux} does not to apply for this model.
Also, ignoring the random noise on the initial distributions, does not inhibit the initiation of the PAT, which is indicated by the unchanged auxin concentration at steady state.
The reason for this might be systematic, since every numerical error in the calculation of any concentration could lead to a sufficient asymmetry, that starts the process.\\


\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/molx/Data_A.png}
\caption{Simulations under moleculeX standard conditions with increase of the auxin base concentration $A$ from 1 to 50.}
\label{fig:molx_A}
\end{figure}

%- initA:	increase height -> increase number, instant decrease height, -> repeat

{\Large Model parameter}\\
The following tested parameter could all be found in the section of the mathematical definition of the models [\ref{sec:1Dequation}, \ref{sec:apoequation}, \ref{sec:molxequation}].\\

 
% A:  		 only total auxin increase
The effect of the auxin base concentration $A$ on this model is not different to the "Simple" model \ref{fig:molx_A}.
Except the increase of the total concentration, which could be explained by the rise of the peak heights and the lowest concentration while the peak width is staying equal, nothing is affected.


%Ga:		decrease height, increase lowest auxin, if<0.015 fewer, broader peaks

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/molx/data_Ga_01_001.png}
\caption{Simulations under moleculeX standard conditions with change value for the auxin sythesis coefficient $G_{a}$. The appearance of the pattern differs significantly, if $G_{a}$ is lower (A) or higher (B) than a value of 0.015.}
\label{fig:molx_Ga}
\end{figure}

In contrary, the increase of the synthesis coefficient $G_{a}$ leads to the opposite effect.
The peak heights are decreasing, while the rest of the pattern remains unchanged, though the mark for lowest concentration is slowly rising.
Interestingly, values below 0.015 produce distant, broad peaks with nearly no auxin in the cells between \ref{fig:molx_Ga}.\\


%Da: 		increase width/lowest auxin, decrease height
Similar behavior can be observed by increasing $D_{a}$, but with the peaks also become broader, which is equal to the results of $D_{a}$ in the analysis of the "Simple" model.\\


%Ep: 		no reaction if <0.3, increase height (saturate 5), decrease lowest auxin, decrease diff of cell and apo pattern
As explained before, the two opposing "forces" of the concentration-based PAT are the "with-the-gradient"-diffusion and "up-the-gradient"-PIN transport of auxin.
According to this, $E_{p}$ has the opposite effect of $Da_{a}$, what matches to the behavior of the model, if $E_{p}$ is varied.\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/molx/Data_Gp_ss_tol.png}
\caption{Simulations under moleculeX standard conditions with the increase of the PIN synthesis coefficient $G_{p}$. The calculation of the steady state depends on a tolerance as indicator for the comparison of two integrational states. (A) With a tolerance of $1e-3$,the smallest value for $G_{p}$ for which a steady state could be calculated is $0.06$. (B) Increasing the tolerance to 1e-4, the calculation of the steady state become possible for $G_{p}=0.01$ }
\label{fig:molx_Gp}
\end{figure}

%Gp: 		no effect (A changed)
At the same time, a change of the PIN synthesis coefficient $G_{p}$ doesn't affect the final pattern at all.
An explanation could be, that $G_{p}$ relates more to the recycling rate of PINs between the membranes and endosomal compartments of the cell, where synthesis and degradation takes place \ref{adamowski2015}, than the synthesis itself. 
Thus, it handles the rate, on how fast the PIN re-distribute and not, on how strong the regulation of this process is.
This could be seen by the decreasing time of the simulation to reach steady state, while $G_{p}$ is increased.
Though, for values below $0.06$, the test results are far too short (i.e. $t=5$ for $G_{p}=0.05$).
The reason for this lies in the termination condition of the function, that calculates the steady states, and it is defined as a tolerated difference between to subsequent states of the integration.
Below $0.06$, the rate, at which the PIN and auxin concentration change is smaller than the selected tolerance and the function falsely assumes the time of the steady state to be shortly after the start of the simulation.
A 10-fold reduction of this tolerance, already results in better resolution \ref{fig:molx_Gp}
Then, from $G_{p}=0.06$ on, it shows a reasonable time ($t=493$) and gradually getting smaller.\\

%m:  		after 2.6: increase number, decrease height (at 3.2, 3.6)
Further, $m$ acts as a factor for the regulation of the PIN distribution and therefore, leads to the increase of peaks at higher values.
Between that, the peak heights slightly grow, but not strong enough to compensate the height loss, that comes along with the generation of a new peak \ref{fig:}.\\ 

%Eq: 		increase height/lowest auxin, decrease apo peaks/lowest apoplast
Since the AUX carrier are assumed not to re-distribute as dynamical as the PINs, the rate of the "apoplast-to-cell" flux by AUX is more or less fixed and only depends on the apoplast auxin concentration.
Thus, if the PIN-mediated flux becomes very strong, the flux by AUX and diffusion would not be enough to reach an equal rate and auxin would accumulate in the apoplast, what could be seen in the simulations of the "Apoplast" model \ref{fig:apo_standard}.
However, the "MoleculeX" model could produces a regular pattern and the fluxes are not equal, but in total they are balanced.

Like the efficiency of PINs, $E_{q}$ is the most important factor, that has an effect on the flux rate.
In this model, even without the AUX flux, a regular patter will be produced, but the concentration of auxin in the apoplasts is nearly twice as much as in the cells.
Increasing it gradually, reduces this difference and at $E_{q}=1$, both distrbutions are aligned (see \ref{fig:molx_standard}).\\
The AUX concentration $q$ is more comparable to an initial factor, rather than a model parameter, but the concentration of AUX inside the plasmamembranes is assumed to be the same, why it is grouped to the model parameter.
Nevertheless, it generates the same effect as $E_{q}$.\\

%V: 		decrease height/lowest auxin/width (~>5),increase number
Another interesting behavior could be seen by the change of the apoplast-cytosol volume ratio $V$, which should normalize the fluxes, because normally, the size of the apoplast, compared to the size of a cell, is by far smaller.
So, flux rates are adjusted to the space size, from where they come.
Increase of $V$ reduces the "apoplast-to-cell" flux and leads to a strong decrease of the peak heights with, contrary to previous observations, an additional increase of the peak number.
Normally if the auxin concentration rised and peaks becoming higher, at a certain point, a new peak is produced, thus revealing an rather unusual behavior, that was not seen in the analysis of the previous model.\\

%Gx: 		increase height/number (limit 5), decrease width, equally for cell/apo/molx
Furthermore, the main difference to the "Apoplast" model is the introduction of the diffusable moleculeX, which seems to contribute a more important part to the patterning the AUX carriers or diffusion.
The increasing of the synthesis coefficient $G_{x}$ has a similar effect as $G_{a}$, that the peak height increases, but also shows its relation to the peak number, where a new peak leads to an instant reduction of all peak concentrations.
In addition, the total peak height is approaching a constant value, which already could be observed for the analysis of the cell number \ref{fig:molx_cn}.\\

%Dx: 		similar to Da, flat pattern >65 
Like $G_{x}$ to $G_{a}$, $D_{x}$ is equivalent to $D_{a}$ and leads to the flattening of the pattern, which creates a completely even distribution over a value of 65 under standard conditions.\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/molx/Data_r_mx.png}
\caption{Simulations under moleculeX standard conditions with a change of regulation factors $r$ (A) and $mx$ (B) for moleculeX synthesis and PIN regulation, respectively.}
\label{fig:molx_rmx}
\end{figure}

%r:  		flat pattern <2.4, increase height,decrease lowest auxin
%mx: 		similar to r, flat pattern <1.6, +decrease width
Finally, the two regulational factors $r$ for the auxin-dependent synthesis of moleculeX and $mx$, equivalent to $m$, for the regulation of the PIN distribution.
Surprisingly, both have the same effect on the simulation with the only difference, that for $r$, values under 2.5 resolve in a flat pattern, whereas for $mx$, they has to be under 1.7 \ref{fig:} \\
\\

After all, the integration of "moleculeX" could reproduce a regular pattern and the updated model features a similar stability to the "Simple" model.
Most parameters, that were tested in this section, show don't have an influence on the distribution process of auxin itself, but mostly affect the total concentration or its ratio between apoplasts and cells.
Some effects, like the instant shifts of the peak heights and lowest auxin concentration, if the number of peaks changes, could be observed in both, the "Simple" \ref{fig:1dsimple_cn} and this \ref{fig:molx_cn} model.
Or some general behaviors, like the opposing fluxes with and against the auxin concentration gradient, that are mainly responsible for the generation of a regular pattern, that could even be shown in the irregular "Apoplast" model \ref{fig:apo_epda}.


\bibliographystyle{plain}
\bibliography{master-ref2}

\end{document}