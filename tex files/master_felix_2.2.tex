\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
%\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{makeidx}
\usepackage{float}
\usepackage{graphicx}
\graphicspath{{../../graphics/}}
\usepackage{wrapfig}
\usepackage{acronym}
\usepackage[onehalfspacing]{setspace}
\usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{hyperref}
\setlength{\parindent}{0pt}
\newcommand{\comment}[1]{}


\author{Felix Risse}
\title{Computational models of pattern generation during developmental processes in plants}
\date{2020}

\comment{
 acronyms
\newglossaryentry{}{}
{TIBA}{2,3,5-trilodobenzoic acid}
{PBC}{periodic boundary condition}
{PAT}{polar auxin transport}
{IAA}{Indole-3-acetic acid}
{ABP}{Auxin binding protein}

{$\Phi$}{Phi}
{$\Theta$}{Theta}
}

%\makeglossaries
\begin{document}

\addtocounter{section}{2}


\subsection{Apoplast model}
\label{sec:apomodel}

\begin{figure}[h!]
\centering
\includegraphics[width=\textwidth]{/apoplast/fujita2.png}
\caption{Schematic of the one-dimensional "Apoplast" model, which is based on the "Simple" model. Apoplast 'cells' are integrated between neighbored cells with an individual auxin concentration ($A_{i,i+k}$). Apoplast doesn't contain PINs and are only able to derade auxin. The PIN distribution of a cell now is dependent on the relative auxin concentration of the neighboring apoplasts instead of the cells. Auxin can still be transported via diffusion or PINs and additionally, by AUX carriers from apoplasts back into cells. Other than PINs, the concentration of them is assumed to be fixed for each membrane.}
\label{fig:Apo_transport}
\end{figure}

The first model \ref{tab:1DSimple} is able to shows the simplest form of a self-organizing pattern. 
One main assumption is the direct contact of each cell to it's two neighboring cells, which may be adaptable for non-plant cells \cite{citi2019}, but tissues in higher plants have an extracellular space and cell walls between individual cellmembranes, the apoplast. 
It's volume is significantly smaller than that of the cytosol (or symplast), but still is an obstacle in the direct path between subsequent cells, since it has to enter and exit the apoplast to reach the other cell. 
Plasmodesmata are a possible way to create a direct connection between cells ("symplast pathway reference"). \\
The transport in the apoplast is assumed to be relatively slower than in the milieu of the cytosol and, therefore, will be handled as a different type of cell in the model. 
The auxin flux over the symplast pathway is not that different to the diffusion between cells in the "Simple" model and is not separately added to the model. \\
Additionaly, independently distributed AUX influx carrier on the apoplast side of the membranes can transport auxin from the apoplast back to the cytosol.

\comment{
\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{apoplot.png}
\caption{One-dimensional model of 100 cells (blue) with extracellular space, the apoplast (orange), in between; each bar indicates the concentration of auxin at it's location and the initial distribution of auxin is shown by the red line.}
\label{fig:Apoplast}
\end{figure} 
}

\subsubsection{Mathematical equations}
\label{sec:apoequation}

Apoplast "cells" also have an auxin concentration and instead of the neighboring cell, cytosol "cells" will exchange auxin with the apoplast "cell". 
An apoplast $a'_{i,j}$ lies between the cells $i$ and $j$. \\

%Eq3
\begin{equation}
\large \frac{da_{i}}{dt}=G_{a}(A-a_{i})-\sum_{j} f_{i,j} +\sum_{j} D_{a}(a'_{i,j}-a_{i}) \label{eq:3}
\end{equation} 
\addtocounter{equation}{-1}

For an apoplast between a cell $i$ and its neighboring cell $j$, $a'_{i,j}=a'_{j,i}$ is denoted as the auxin concentration, $q$ as a factor for the abundance of AUX proteins in the apoplasts and $Eq$ as the control parameter for their efficiency. 
The synthesis term is from the equation of the "Simple" model, since this process should not be affected by the addition of extracellular space.

\begin{subequations}
\begin{align}
\large   f_{i,j}=E_{p}p_{i,j}a_{i}-E_{q}qa'_{i,j} + \sum_{j} D_{a}(a'_{i,j}-a_{i}) \label{eq:3a}
\end{align}
\end{subequations}

The PIN-mediated efflux $E_{p}p_{i,j}a_{i}$ also stays the same, but ,as mentioned before, the auxin influx $E_{q}qa'_{i,j}$ comes from the apoplast instead of the other cell and now depends mainly on the apoplastic auxin concentration $a'_{i,j}$.

%Eq4
\begin{equation}
\large \frac{p_{i,j}}{dt}=G_{p}\left(Kp\frac{\Phi(a'_{i,j})}{\sum_{j}\Phi(a'_{i,j})}-p_{i,j}\right) \label{eq:4}
\end{equation}

Also, the ODE for the PIN is the same as in the first model, except the substitution of the cytosolic auxin concentration $a_{j}$ with the apoplastic $a'_{i,j}$.
The concentration change of auxin in the apoplast is described by

\begin{equation}
\large \frac{da'_{i,j}}{dt}=-G_{a}a'_{i,j}+\frac{1}{V}(f_{i,j}+f_{j,i})+\frac{D_{a}}{V}(a_{i}+a_{j}-2a_{i,j}) \label{eq:5}
\end{equation}
\addtocounter{equation}{-1}

and resembles the auxin equation of the cytosol.

\begin{subequations}
\begin{align}
-G_{a}a'_{i,j} \label{eq:5a} \\
\frac{1}{V}(f_{i,j}+f_{j,i}) \label{eq:5b} \\
\frac{D_{a}}{V}(a_{i}+a_{j}-2a_{i,j}) \label{eq:5c}
\end{align}
\end{subequations}

The apoplast is not able to synthesize auxin by itself, so auxin can only be degraded \eqref{eq:5a}. 
The factor $\frac{1}{V}$ in \eqref{eq:5b} and \eqref{eq:5c} is for the normalization of the volume difference between cyto- and apoplast, where the $V$ is the cytoplast:apoplast volume ratio. 
The PIN-mediated flux \eqref{eq:5b} and diffusion \eqref{eq:5c} will be calculated between the apoplast and it's neighboring cells $i$ and $j$.

\subsubsection{Analysis}
\label{sec:APOmbt}

\comment{
\begin{figure}
\centering
\includegraphics[scale=.5]{/apoplast/.png}
\caption{}
\label{fig:}
\end{figure}
}

\begin{figure}
\centering
\includegraphics[scale=.5]{/apoplast/standards_new_old.png}
\caption{Simulations of the "Apoplast" model with the previous standard conditions for the inherited parameters of the "Simple" model (\textbf{A}) and a new standrd parameter set (\textbf{B}). Auxin concentrations shown for each cell (blue) or the extracellular space between two cells (orange).}
\label{fig:apo_standard}
\end{figure}

Even without testing, the standard simulation of the "Apoplast" model is far different from the one of the "Simple" model \ref{fig:apo_standard}.
This does not simply occur, because of the new selected standard values, since execution of this model with the previous parameter set results in a stable steady state with even auxin distributions.
However, all model parameter, which are defined in the "Simple" model \ref{sec:1Dequation} are also used in this model plus three additional \ref{tab:Apo_standard}.
Further tests with these new parameters and the old parameter set will be discussed later in this part.
The space between cells, the apoplast, has the same total size as the internal cell space, the cytosol, but has a different behavior.
In the previous model, the pattern could evolve, because each section of the system was identical.
In the current model, the sections alternate between two types, which is probably the reason, why a the simulations may not result in a regular pattern like before.\\
The testing of the "Apoplast" model is similar to before.
Additionally, the question, if it is possible to re-generate a regular pattern like in the previous model just by tuning the parameter values, will also be discussed later.\\
\\
Since the earlier testing values don't consider the apoplast concentrations, three new tests are added: the number of apoplast peaks, the lowest apoplast auxin concentration and average height of apoplast peaks.\\
An apoplast peak is defined by a concentration, that is higher than both adjacent cells and the neighboring apoplasts.
Calculation of the minimal concentration and apoplast peak height is equivalent to the cytosol test values.\\
\\

\begin{table}
\centering
\begin{tabular}{|r|r|r|}
\hline
Variable&Value	&Definition\\
\hline
\multicolumn{3}{|c|}{Conditions}\\
\hline
N		&50		&total cell number\\
K		&2		&number of adjacent cells per cell\\
Auxin	&6		&initial auxin concentration per cell\\
PIN		&40		&initial PIN concentration per membrane side per cell\\
Time	&100000	&maximal integration time\\
Noise	&0.01	&relative noise on initial values\\
PBC		&1		&boundary condition\\
\hline
\multicolumn{3}{|c|}{Parameter}\\
\hline
$G_{a}$	&1	&auxin synthesis coefficient\\
A		&6		&auxin base level\\
$D_{a}$	&30.5	&auxin diffusion coefficient\\
$G_{p}$	&2		&PIN synthesis coefficient\\
P		&40		&PIN base level\\
m		&4		&PIN regulation factor\\
$E_{p}$	&1		&PIN efflux efficiency coefficient\\
q		&10		&AUX concentration per membrane\\
$E_{q}$	&1		&AUX influx efficiency coefficient\\
V		&1		&apoplast-cytosol volume ratio\\
\hline
\end{tabular}
\caption{Values and definitions of the parameter used as the standard for analysis of the "Apoplast" model.}
\label{tab:Apo_standard}
\end{table}

Incrementing the number of cells means in this case, one cell and one apoplast are added to the system.
Thus, the sequence of this model starts with a cell and ends with an apoplast.
This is just made to simplify the handling of this model, especially if it has the periodic boundary condition.\\
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.45]{/apoplast/Data_cn.png}
\caption{Simulations under apoplast standard conditions with size varying over a range from 5 to 100 cells. (\textbf{A} and \textbf{B}) Testing results were taken at steady state for each cell number $N$; (\textbf{C}) Simulation with system of 95 cells. Even without a global pattern, similar structures (red and green frames) can be found. }
\label{fig:apo_cn}
\end{figure}

The system size is not affecting the simulation significantly. 
No global pattern can be observed, however, some subsequences look remarkably similiar (\ref{fig:apo_cn}.
This could imply, that the initiation of patterns happens locally, but the global structure of the "Apoplast" model prevents the generation of a full pattern over the complete system.\\
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.4]{/apoplast/Data_init_auxpin.png}
\caption{Simulations under apoplast standard conditions with initial values for auxin and PIN in a parameter range from 1 to 96. Test results for (\textbf{A}) initial auxin and (\textbf{B}) PIN. Results of the newly added tests are not shown, since they don't show any helpful information.}
\label{fig:apo_init}
\end{figure}

A different initial values for the auxin concentration leads to no noticeable effect in the simulation (\ref{fig:apo_init}.
The behavior for a initial auxin concentration over 66 $\mu mol/\mu m^{3}$, where several testing values simultaneously switch to another value could also be found in the testing results of the previous model \ref{fig:1dsimple_m}.
No significant difference is observable in the simulations whatsoever.\\
Like the inital auxin concentration, the initial PIN concentration also doesn't affect the process much.
For a PIN concentration under 30 $\mu mol/\mu m^{3}$, the testing values slightly fluctuate, which could be a result from a lower initial value than the PIN base level $p$.\\
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{/apoplast/data_noi.png}
\caption{Simulations under apoplast standard conditions with 20\% and 50\% initial noise.}
\label{fig:apo_noi}
\end{figure}

Variation of the noise in the initial auxin or pin distribution has no effect on the simulation \ref{fig:apo_noi}.\\
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.5]{/apoplast/data_bc.png}
\caption{Simulations under apoplast standard conditions with (\textbf{A}) "cutoff", (\textbf{B}) "PBC" or (\textbf{C}) "zero-flux" boundary condition.}
\label{fig:apo_bc}
\end{figure}

As observed before, the "Apoplast" model itself may prevent the expansion of the pattern or, at least, pattern-like structures \ref{fig:apo_cn} and the different conditions doesn't affect most of the simulated system. 
However, the influence of the boundary condition can be seen clearly at the margins of the simulations \ref{fig:apo_bc}.
A system with the "cutoff" condition has a strong decrease towards the borders of the system with the "lowest auxin concentration" of 0.15 instead of 0.80 for both of the other.
In the "Simple" model, the difference between the "PBC" and "zero-flux" is very distinct, since the pattern either continues "over the borders" in the "PBC" condition or not, if it has the "zero-flux" condition \ref{fig:1dsimple_bc}.
Without a pattern, this conditions could not be distinguished that easily by comparing the simulations, but simulations with a "zero-flux" condition often results in a significant higher auxin concentration in the left terminal cell.
This behavior is also present for the previous model.\\
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.5]{/apoplast/Data_ss_change2.png}
\caption{Relative difference in the distribution of auxin to a standard simulation. Source or sink is at position 25 (red square) in an apoplast (\textbf{A} and \textbf{B}) or a cell (\textbf{C} and \textbf{D}).}
\label{fig:apo_ss}
\end{figure}

The effect of an external flux by applying an auxin source or sink at one position of the model only affects near concentrations (around 5-10 cells/apoplasts) compared to the previous model.
Although, none of the calculated test values show interesting results, comparing the concentrations to the standard model without a source or sink, some interesting observations can be made \ref{fig:apo_ss}.
Sinks produce a strong increase in concentration to their left and sources to the right, but always a slight decrease at their position.
Whereas a source or sink in a cell doesn't affect their surroundings and only generate a one high or negative concentration.
The reasen for this behavior could not be explained.\\
\\

{\large Model Parameter} \\
The following tests will use the choosen standard for the initial values and model parameters \ref{tab:Apo_standard} and only the tested parameters will be changed.
\\

\comment{
\begin{figure}
\centering
\includegraphics[scale=.6]{/apoplast/.png}
\caption{}
\label{fig:}
\end{figure}
}

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/apoplast/Data_A.png}
\caption{Simulations under apoplast standard conditions with the increase of the auxin base level from 1 to 50 $\mu g/L$.}
\label{fig:apo_A}
\end{figure}

By increasing the base level $A$, the distribution pattern of auxin over the whole system remains unchanged.
However, the height of the peaks and the lowest auxin concentration increase linearly with the changed base level \ref{fig:apo_A}.
This is not unexpected and comparable to the results of the "Simple" model, since every cell tries to maintain this base level.\\
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/apoplast/Data_Ga.png}
\caption{Simulations under apoplast standard conditions with increase of the auxin synthesis coefficient $G_{a}$ from 0.05 to 10. $G_{a}$ does not change the rate, but how strong it is affected by the difference of the base level $A$ and the actual concentration.}
\label{fig:apo_Ga}
\end{figure}

Moreover, changing the coefficient $G_{a}$ of this synthesis rate, also shows an increase of the lowest auxin concentration, but a simultaneous decrease of the calculated peak height \ref{fig:apo_Ga}.
The rate of the cell-internal auxin synthesis depends on the difference between the auxin concentration and the base level $A$ and $G_{a}$ is just a factor for the effect of this difference on the final rate. 
Also does a value under around 3.0 for $G_{a}$ show some reaction for the auxin peak number and distance, where values over 3.0 affect the outcome of the simulation not at all or only slightly (i.e. "avg. peak width" in \ref{fig:apo_Ga}).\\
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/apoplast/Data_Da.png}
\caption{Simulations under apoplast standard conditions with the increase of the diffusion coefficient $D_{a}$ from 0 to 120. Maximum of "avg. peak height" if $D_{a}$ between 10 and 20.}
\label{fig:apo_Da}
\end{figure}

Similar to $G_{a}$, an increase of the diffusion coefficient increases the minimal concentration and flattens the peaks \ref{fig:apo_Da}, however, the peak height shows an unexpected maximum for $D_{a}=19.6$, while the average peak concentration of the apoplasts is permanently decreasing.
Even if not significantly big, it states, that the increase of the diffusion rate at a low level could increase the cytosolic auxin concentration relative to the concentration in apoplasts.
Further, the value for the diffusion coefficient at which the calculated peak height is at its maximum may be related to the rate of the flux of the PIN-mediated transport. Since the direction of diffusion only depends on the concentration gradient, at least in this model \ref{eq:1}, and the PAT is "up-the-gradient", there is an equilibrium point with an auxin netflux of zero, which is determined by the ratio of these two opposing fluxes.\\
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/apoplast/Data_Ep_0255.png}
\caption{Simulations under apoplast standard conditions with variation of the PIN efficiency $E_{p}$ from 0 to 1. (A) Simulation with an value of 0.255 for $E_{p}$. Higher ($E_{p}=0.256$) or lower ($E_{p}=0.254$) values either show increasing irregularity or no change at all, respectively. (B) Calculated test values of steady states with different $E_{p}$. (C) Calculated test values of steady states with $E_{p}=0.255$ and variation of $D_{a}$. Red marks indicates values at which the results drastically change.}
\label{fig:apo_Ep}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/apoplast/Data_epda.png}
\caption{Simulations under apoplast standard conditions with the parameter $E_{p}$ against $D_{a}$. The heatmap shows the difference at steady state to the result with $E_{p}=0.255$ and $D_{a}=30.5$, that shows a regular pattern.
The difference of two distributions is defined by the standard deviation of the element-wise, absolute difference in the auxin concentration. The red dot mark the smallest value for a specific $E_{p}$. Calculated linear regression revealed a change of 12 of the diffusion coefficient per increase of the PIN efficiency by $1\%$.}
\label{fig:apo_epda}
\end{figure}

As a control parameter for the PIN-mediated transport, $E_{p}$ defines the efficiency and is assumed to have a value between 0 and 1.
So, an $E_{p}$ of 0 would repress the transport, where a value of 1 would make the rate just dependant on PIN and auxin concentrations \ref{eq:1}.
Interestingly, with an efficiency of 0.255, a regular pattern could be generated, while with lower values, it is not really different from the initial distribution and higher values gradually create a more irregular outcome \ref{apo_Ep}.
Although, the value for the standard deviation of the peak distances is clearly above zero.
Since the only change from the initial distribution is the alternating auxin concentration in the apoplasts and concentrations in the cells are nearly on the same level, the peaks are not detected correctly and therefore, related test values are more or less redundant.
But an increasing PIN efficiency stabilizes the distribution and leads to an increase of the peak height and decrease of the lowest concentration.\\
Furthermore, setting $E_{p}$ to 0.255 and varying $D_{a}$ from 0 to 100 also shows this significant change of the test values around 30.5, the standard value of $D_{a}$.
However, lower values for $D_{a}$ increase the peak height and decrease the lowest auxin concentration, while a higher value leafs the concentrations unaffected, but again creates noisy results for the peak-related tests.  \\
To find a correlation between these parameters, the standard deviation of the element-wise difference in the auxin concentration was calculated for $E_{p}$ (0-1) against $D_{a}$ (1-50) \ref{fig:apo_epda}.
Unfortunately, the peak height of the reference is relatively small and a high diffusion rates consequently lead to a flat distribution, thus, it is not clearly detectable, if it is similar or just flat.
Though, by determining the lowest value per $E_{p}$ (red marks;\ref{fig:apo_epda}), the regression could be calculated to maintain a relatively similar distribution.
This could be observed by applying it to the reference.\\
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/apoplast/Data_m.png}
\caption{Simulations under apoplast standard conditions with the increase of PIN regulation coefficient $m$, which affects the influence of neighboring auxin concentrations on the PIN distribution of a cell [\ref{phi}].}
\label{fig:apo_m}
\end{figure}

Furthermore, the increasing volume ratio $V$ between apoplasts and cells results in a decrease of the total auxin concentration, which goes with a reduction of the peak heights and the lowest auxin concentration, but simultaneously a higher peak number. 

At the same time, the only effect of the AUX influx carrier by manipulating the general membrane concentration $q$ or its efficiency $E_{q}$ is an increase of the total cytosolic auxin concentration, but shows no significant effect on the distribution and therefore no significance for the pattern generation.
Likewise, the remaining parameter $G_{p}$ for the PIN synthesis also leads to no interesting effect regarding the process of the pattern generation, except the in- or decrease of the global auxin concentration.\\
Just the regulation coefficient $m$ for the PIN distribution has a small effect on the auxin distribution for values between 2 and 2.5, but otherwise, only increases the peak height by reducing the lowest auxin concentration \ref{fig:apo_m} for values over 2.



 


\bibliographystyle{plain}
\bibliography{master-ref2}

\end{document}
