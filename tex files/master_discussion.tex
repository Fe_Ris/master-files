\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
%\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{makeidx}
\usepackage{float}
\usepackage{graphicx}
\graphicspath{{../../graphics/}}
\usepackage{wrapfig}
\usepackage{acronym}
\usepackage[onehalfspacing]{setspace}
\usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{hyperref}
\setlength{\parindent}{0pt}
\newcommand{\comment}[1]{}

\comment{
\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/molx/.png}
\caption{}
\label{fig:}
\end{figure}
}

\author{Felix Risse}
\title{Computational models of pattern generation during developmental processes in plants}
\date{2020}


\begin{document}

\section{Models}
\label{sec:models}


\subsection{Simple 2D model}
\label{sec:2d}

\begin{equation}
\large \frac{da_{i,j}}{dt}=G_{a}(A-a_{i,j})-\sum_{k}^{4} f_{i,j,k} +\sum_{k}^{4} D_{a}(a_{k}-a_{i,j}) \label{eq:2d_1}
\end{equation}
\addtocounter{equation}{-1}
\begin{subequations}
\begin{align}
\large  f_{i,j,k}=E_{p}(p_{i,j,k}a_{i,j}-p_{k,i,j}a_{k}) \label{eq:2d_1a}
\end{align}
\end{subequations}

\begin{figure}[h!]
\centering
\includegraphics[width=\textwidth]{/2d_view_quarter+netlogo.png}
\caption{Two-dimensional model based on the "Simple" model. (A,B) The model is build on a 50x50 rectangular grid with the height (z-axis) as the absolute auxin concentration at each position. Used values for the parameters are [auxin=1, pin=10, noise=0.1, PBC=1, A=4, Ga=0.2, Da=1.31, P=40, Gp=0.25, Ep=0.2]. (A) Perspective and (B) Top view of the final simulation. (C) Alternative representation (extract, inverted) of the model. Each cell is divided into a central (cytosol) and four marginal spaces (intermembrane PIN), where the opacity of a space is defined by the respective concentration of auxin (green) or PIN (red). (D) Agent-based model for the two-dimensional case of the patterning process. Model size is 21x21 with the auxin concentration presented by the normalized grayscale color of each cell.}
\label{fig:2d_view}
\end{figure}

As explained, the one-dimensional model is sufficient to describe and present the self-maintained pattern generation of the auxin distribution along a tissue.
Together with the periodic boundary condition (PBC), this concentration-based model fits to the biological reference of the peripheral zone of the SAM, which is basically a ring of a single cell layer.\\
Nevertheless, the mathematical model can easily expanded two support a two dimensional model, where basically only the number of neighboring cells increases, if we assume the condition of the "Simple" model and a rectangle grid of cells.
Keeping the discretization of the spaces, this even could also be calculated with simple ordinary differential equations.\\
On the other hand, the implementation of this approach results in a long computation time of single simulations simulations, increasing more than exponentially with the size.
Hence, as seen in the previous analyses, the size of the model has a high influence on the pattern, that can be produced, as a pre-condition for a useful model for the analysis of its behavior, the size of the model should be sufficient big.
This makes an analysis rather tedious and therefore, an additional analysis of a two-dimensional model is not included in this work.\\
However, simulations with the parameter defined and a smaller size are possible \ref{fig:2d_view}. \\
\\
The process of the auxin distribution in the 2D model is identical to the "Simple" model, where each cell is directly connected to every neighboring cell. 
Even without the analysis of parameters, like it was done for the one-dimensional models, some interesting observations can be derived from a single simulation.
First, the auxin peaks are thinner than in the "Simple" model, but if this just depends on the selected parameters,the change of the model structure or the mathematical equations is unclear yet.
Further optimization of the  present version has to be made to make it feasible for an more detailed analysis.\\
Another aspect of the two-dimensional simulation is the arrangement of peaks.
Other than results of the "Simple" model, the localization of the peaks is not as ordered as expected, it looks rather cramped (\ref{fig:2d_view}A-B).
Again, at this state it is hard to find a conclusion for this behavior, but one possibility could be the basis of the model structure.
More optimal as a representation of a two-dimensional cell layer in a tissue would be a grid with hexagonal cells. 
Beside the irregularity of the peak arrangement, it seems to develop a pattern, that might not fit to the rectangular orientation.
As far as by now, further assumptions based on a single simulation with a fixed set of parameter, are not very useful.\\
Nevertheless, the distribution of PINs (\ref{fig:2d_view}C) in this model is as expected, which is a sign of the process working as it should, leading to uni-directional auxin fluxes to distinct converging points for auxin.\\
\\
To find a possible solution to the problem of the model optimization, another approach for the creation of a two-dimensional representation of the process was an agent-based model.
The main difference to the previous models is the individual calculation of the concentrations per cell instead of stepwise for the whole system.
Each cell is an individual "agent" with a fixed position and interacts individually with other the other "agents".
Since the spaces of the model has no need to be continuous and the general reaction of each cell is the same, this was a possible approach.
However, changing from a system-wide integration to an individual calculation of the concentration change per cell, could create a pattern, but apperared to be even more computation-intensive.
Since this doesn't help to overcome the initial problem, the agent-based model was not further investigated.\\


\subsection{Other models}
\label{sec:other}

The analyzed main models, that are based on the work of Fujita et al. \cite{fujita2018} are sufficient to present the process of the pattern generation, that is maintained by a relatively simple feedback regulation.
Beside this models, there exists a lot more, that are related to the polar auxin transport \ref{other_models}.
Although, the process of the auxin pattern generation in this type of model highly depends on the regulated, asymmetric distribution of PINs in a cell, how the auxin concentration outside the cell could be detected, still remains undefined.\\
In order to find possible explanations, other existing models, that are related to the PAT were investigated.

\subsubsection{ABP model}
\label{sec:ABP}
% Adamowski2015

The first example describes the regulation of the endocytosis rate of the membrane-bound PINs by apoplastic, auxin binding proteins (ABP) \cite{adamowski2015}.
The apoplast between two neighboring cells contains a pool of auxin binding proteins (ABP1), which can in their unbound state lead to an increase of the endocytosis of PINs from the membrane.
However, they loose this ability if auxin is bound.
Due to an auxin concentration gradient across the apoplastic space, different amounts of ABP are bound to auxin at the distinct sides.
The remaining unbound ABP promote the clathrin-mediated endocytosis of PINs at their respective location, which results in an increased asymmetry of active PIN tranporters between the both cells.
Thus, more PINs remain bound to the membrane, which has the higher auxin efflux into the apoplast and \textit{vice versa}.\\

Although, this supports the flux-based type of the PAT, it reveals potential for a theoretical hypothesis, that could explain the detection of the external auxin concentration in the concentration-based models.

  


\subsubsection{Leaf margin morphogenesis}
\label{sec:LMM}
% Tameshige2016

\subsection{Conclusions}
\label{sec:conclusion}

- model process


\subsection{Outlook}
\label{sec:outlook}



\bibliographystyle{plain}
\bibliography{master-ref2}

\end{document}