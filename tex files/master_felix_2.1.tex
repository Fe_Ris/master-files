\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
%\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{makeidx}
\usepackage{float}
\usepackage{graphicx}
\graphicspath{{../../graphics/}}
\usepackage{wrapfig}
\usepackage{acronym}
\usepackage[onehalfspacing]{setspace}
\usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{hyperref}
\setlength{\parindent}{0pt}
\newcommand{\comment}[1]{}


\author{Felix Risse}
\title{Computational models of pattern generation during developmental processes in plants}
\date{2020}

\comment{
 acronyms
\newglossaryentry{}{}
{TIBA}{2,3,5-trilodobenzoic acid}
{PBC}{periodic boundary condition}
{PAT}{polar auxin transport}
{IAA}{Indole-3-acetic acid}
{ABP}{Auxin binding protein}

{$\Phi$}{Phi}
{$\Theta$}{Theta}
}


\begin{document}


%\pagenumbering{roman}
\tableofcontents
\clearpage


\section{Models}

The following one-dimensional models are based on the work of \textit{Fujita et. al} \cite{Fujita2018}and the term "one-dimensional" refers to the arrangement of the cells in these models. 
They intended to give an insight into self-organizing, regular patterns in multicellular organism (i.e. phyllotaxis). 

\subsection{One-dimensional auxin pattern model}
\label{sec:1Dmodel}

\begin{figure}[h!]
\centering
\includegraphics[width=\textwidth]{1dsimple/fujita1.png}
\caption{Schematic of the one-dimensional "Simple" model. Adjacent cells are directly connected and each cell in the sequence has its own internal auxin concentration ($A_{i}$) and a PIN concentration ($PIN_{i}$), that is asymmetrically distributed between both sides of the plasma membrane plasmamembranes. Auxin is produced and degraded in the each cell and can pass the membranes either by simple diffusion (blue arrows) or unidirectional via active transport by the PINs inside of the membrane. PINs are constantly cycling between membranes and endosomal compartments of the cell for synthesis and degradation. The regulation of the PIN re-distribution depends on the highest auxin concentration of the neighboring cells.}
\end{figure}

In order to simulate the regular pattern generation of auxin along multiple cells, driven by the \hyperref[sec:PAT]{PAT}, a model system with a one-dimensional cell layer, PIN proteins and Auxin is used. 
The whole model is build from a row of square cells, which all are based on the same cell type and only differ by the magnitude of their features (i.e. cytosolic auxin concentration). 
There are two possible conditions for the border: a \hyperref[sec:PBC]{periodic boundary} or a zero netflux, which you can imagine as a neighboring cell with exactly the same efflux to it's side as the outer cell. \\
The simple model just consists of two components, auxin and PIN proteins. 
The process of generating a regular pattern in terms of the distribution of auxin along the cells is mainly based on the interaction of these two molecules and their mutual feedback regulation. 
Auxin can either diffuse from one to a neighboring cell or could be actively transported by PIN proteins in the plasmamembrane. 
Although, according to the \hyperref[sec:CM]{Chemiosmotic model}, auxin should not be able to simply diffuse out of a cell, a possible diffusion is assumed, since the cells in this model have no extracellular space in between and the difference of the pH of environments/compartments is not included as an influencal factor.
On the other hand, PIN proteins are not fixed in their location. 
They are in a constant re-cycling process and transported via vesicle transport between the membrane and vacuole, where they are assumed to degenerate and synthesize. 
This PIN cycling makes the distribution of PIN on the membrane very dynamic and is, therefore, a key to this feedback process, because the location where PINs are re-integrated into the membrane is dependent on the relative concentration of auxin of the neighboring cell in that direction. 
In case of this one-dimensional model with two neighbors per cell, the amount of PINs will be consequently higher on the membrane side, where the other cell has a higher auxin concentration. 
As an effect, more auxin will be actively transported to this side and increase the amount in the neighboring cell even more. 
This netflux can be described as "up-the-gradient", because normally, the gradient of two different concentrations would point to the lower one, leading to the equalization of the conentrations, but the principle of the PIN-mediated auxin flux works exactly in the opposite direction. \\
Like PINs, auxin is synthesized and degraded in each cell. 
Without flux or diffusion of auxin, there will be an equilibrium between the rate of synthesis and degration, which is used in the model as an "base" concentration of auxin. 
This also inhibits the ongoing increase of auxin in some cells by the PIN-mediated flux and at some point, there will be an equilibrium between the increase by flux and decrease towards the base concentration. \\

\begin{figure}
\includegraphics[width=1\textwidth]{1dplot.png}
\caption{One-dimensional auxin pattern model at steady state; auxin concentration of the cells in the model are described by the (blue) bars. Initial distribution of auxin with a random noise of 1\% marked by the (red) dashed line. The total amount of auxin in one period (peak-to-peak) is nearly equal to the initial amount.}   
\end{figure}

\subsubsection{Mathematical equations}
\label{sec:1Dequation}

The mathematical equations for the model come from \textit{Fujita et. al} \cite{Fujita2018}. 
Since the location of a component can be described by a discrete parameter, the simple one-dimensional model could defined by 3 ordinary differential equations, independent of the total size of the simulated system. 
The time for one simulation, although, can increase exponentially for the number of cells in the system. \\

\begin{equation}
\large \frac{da_{i}}{dt}=G_{a}(A-a_{i})-\sum_{j} f_{i,j} +\sum_{j} D_{a}(a_{j}-a_{i}) \label{eq:1}
\end{equation}
\addtocounter{equation}{-1}

For the change of the concentration change of auxin of a cell ($ \frac{dta_{i}}{dt} $), the equation can be divided into three parts by their function: synthesis, PIN-mediated flux and diffusion. 

\begin{subequations}
\begin{align}
\large G_{a}(A-a_{i}) \label{eq:1a}
\end{align}

Each cell has a synthesis and degradation rate of auxin, which are more or less dependent of the actual concentration of auxin inside the cytosol. 
A higher concentration would shift the equilibrium of this rates to the degradation and \textit{vice versa}. 
$A$ is this base concentration or equilibrium constant that determines, if the cell mainly generate or degrades auxin. 
$G_{a}$ is the control parameter for this term.


\begin{align}
\large -\sum_{j} f_{i,j} \quad with \quad f_{i,j}=E_{p}(p_{i,j}a_{i}-p_{j,i}a_{j}) \label{eq:1b}
\end{align}


Both sides of the cell's plasmamembrane inhabit an amount of internal PIN proteins, which actively transport auxin to the neighboring cell. 
The efflux on one side depends on the PIN concentration $p_{i,j}$ on the membrane from the own cell $i$ to the next cell $j$ and auxin $a_{i}$. 
The influx is simply the efflux of the neighbor cell $p_{j,i}a_{j}$ to cell $i$. 
Again, $E_{p}$ is a control parameter for this term ,showing the efficiency of the PIN-mediated transport.

\begin{align}
\sum_{j} D_{a}(a_{j}-a_{i}) \label{eq:1c}
\end{align}
\end{subequations}

The last part of the equation is for the change by diffusion, which basically is a concentration gradient to the subsequent cell for both sides $a_{j}-a_{i}$ and a control parameter $D_{a}$. \\
PINs are described by one equation per membrane side with a neighboring cell with only a term that resembles the synthesis part of the auxin equation.

\begin{equation}
\large \frac{p_{i,j}}{dt}=G_{p}\left(Kp\frac{\Phi(a_{j})}{\sum_{j}\Phi(a_{j})}-p_{i,j}\right) \label{eq:2}
\end{equation}

$p_{i,j}$ is the membrane's PIN concentration, $K$ the number of neighbors and $G_{p}$ a control parameter. 
Instead of just a base concentration, here as $p$, the relative excess of auxin in the subsequent cell compared to every neighboring cell ($\frac{\Phi(a_{j})}{\sum_{j}\Phi(a_{j})}$ is also calculated. 
The regulatory function $\Phi()$ can be used to modify the type and strength of the influence of auxin to PIN. 
For this model, this function is $\Phi(a_{j})=a_{j}^{m}\label{phi}$ with m as an arbitrary parameter.\label{phi} \\

\subsection{Analysis}
\label{sec:1Dmbt}

A computational model of a biological process or system is just a simplification and should be able to behave at least in some features like its origin in nature. For the one-dimensional model, this would be the outer layer (L1) of an shoot apical meristem (SAM) in the growth phase of a higher plant \cite{adamowski2015}. 
In this section, the behavior of the model was tested for different variations of it's initial conditions (i.e. cell number, initial values, etc.) and parameters. 
In each test, at least one variable is modified with every other value kept at a the pre-defined standard.
This standards are not based on experimental results or lituerature due to the lack of reference, but lead to a representative solution.
Altough the mathematical equations of these models may be from other publications, the implementation into a computational model is completely my own work. \\
The same testing procedure is individually adjusted to be able for the usage of the other models, so that the results of all test simulations are comparable.
Detailed information about the code of implemented models and additionally used functions can be found on \href{https://gitlab.com/Fe_Ris/master-files}{GitLab}.
 
 
\begin{table}
\centering
\begin{tabular}{|r|r|r|}
\hline
Variable&Value	&Definition\\
\hline
\multicolumn{3}{|c|}{Conditions}\\
\hline
N		&50		&total cell number\\
K		&2		&number of adjacent cells per cell\\
Auxin	&5		&initial auxin concentration per cell\\
PIN		&40		&initial PIN concentration per membrane side per cell\\
Time	&100000	&maximal integration time\\
Noise	&0.01	&relative noise on initial values\\
PBC		&1		&boundary condition\\
\hline
\multicolumn{3}{|c|}{Parameter}\\
\hline
$G_{a}$	&0.01	&auxin synthesis coefficient\\
A		&5		&auxin base level\\
$D_{a}$	&100	&auxin diffusion coefficient\\
$G_{p}$	&1		&PIN synthesis coefficient\\
P		&40		&PIN base level\\
m		&2		&PIN regulation factor\\
$E_{p}$	&1		&PIN efflux efficiency coefficient\\
\hline
\end{tabular}
\caption{Values and definitions of the parameter used as the standard for analysis of the "Simple" model.}
\label{tab:1DSimple}
\end{table}


The initial conditions of the "Simple" model are the size of the simulated model system (N) and distribution of the two components Auxin and PIN over the whole system.
Since these models are only one-dimensional, each cell can have at most two adjacent neighbor cells (K). 
Other initial parameters, like the boundary condition (PBC) \ref{sec:MS} and noise, which adds distortion to the initial distributions, are modifications of this conditions.\\
There are seven model parameters in this model \ref{sec:1Dequation}, which will be inherited by following models that are based on \cite{Fujita2018}.\\
\\

\comment{
\begin{figure}
\centering
\includegraphics[scale=1]{/1dsimple/.png}
\caption{}
\label{}
\end{figure}
}

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/1dsimple/Data_cn_5_100_v2.png}
\caption{Test results of the "Simple" model under standard conditions with varying size from 5 to 100 cells. Value of the tested variable is plotted against the test values. The left axis is for the steady state and the other for the remaining test values with their unit depicted in the legend.}
\label{fig:1dsimple_cn}
\end{figure}

%66/36/28
By varying the size of the system, it is shown, that under the standard condition, the model does need at least a minimal number of cells to develop it's typical "wave pattern".
This also highly depends on the type of boundary condition \ref{fig:F_bc}.
While a closed system ("zero-flux") and a system, which is theoretically expanded over it's original size ("PBC") show the first occurrence of a second peak by relativly small cell number with 28 and 36 cells, respectively,
an open system ("cutoff") needs to be double that size. 
Clearly observable is the correlation between the peak height, which includes the minimal auxin concentration, and the peak distance \ref{fig:1dsimple_cn}. They show a linear increase, but the number of peaks act like a switch, that determines the slope of the rate at which the peaks grow.\\


\begin{figure}[h!]
\centering
\includegraphics[width=\textwidth]{/1dsimple/Data_iva_aux_switch.png}
\caption{Simulations under standard conditions with variation of the initial auxin value. Steady state pattern of auxin with 20 (\textbf{A}) and 25 (\textbf{B}) $\mu mol/L$. (\textbf{C}) Test results for increasing initial auxin concentration and the base value concentration $A$ set to a fifth of it. }
\label{fig:1dsimple_iva_aux}
\end{figure}

The inital values for auxin and PIN doesn't show any effect on outcome of the simulation, except for auxin, when it is at least five-fold of the base level concentration \ref{fig:1dsimple_iva_aux}. 
If the initial concentration is below this threshold, the resulting pattern is as expected, but if it is higher, it just generates a single, significantly higher, peak. 
Tests over a range of initial concentrations, where the base is always set to a fifth, the peak height is linearly increasing, while other values are not changing at all.
This correlation could not be evaluated for PIN and the parameter $p$, which defines the base level for integrated PIN proteins at one membrane site.
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.5]{/1dsimple/Data_noi_0.png}
\caption{Simulation with standard conditions and no noise.}
\label{fig:1dsimple_noi}
\end{figure}

To initiate the process, there need to be some asymmetry in the distribution of auxin or PIN, since the \comment{\ref{section PAT}{polar auxin transport}} needs a gradient of the auxin concentration between adjacent cells to happen.
Thus, without noise, the system keeps the even auxin distribution and instantly enters it steady stage \ref{fig:1dsimple_noi}.
Other than leading to the initiation of the process, noise has no further effect on the system.
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.4]{/1dsimple/data_bc.png}
\caption{Simulation with standard conditions and the three possible structure modes for the system: (\textbf{A}) an open system, where auxin can leave to the environment, (\textbf{B}) a closed, continuous system and (\textbf{C}) a semi-closed system.}
\label{fig:1dsimple_bc}
\end{figure}

One important factor for the general behavior of the model is the definition of the system boundaries \ref{sec:MS}.
Either if it is in contact with the environment (\ref{fig:1dsimple_bc},"cutoff") or not ("PBC" or "zero-flux").
The "cutoff" condition increases the efflux of the terminal cells and inhibits the generation of peaks near the margins of the system, if it's size is too small.\\
For the one-dimensional model, the system with "PBC" could be imagined as a closed circle of cells.
If the system is sufficiently big, the terminal cells only interact with each other over the border and as a factor for the pattern generation, the system seems broader than it really is.\\
The "zero-flux" condition also leads to a more different result than a regular closed system.
In the case of auxin concentrations, there is no difference, if the influx is equal to the efflux or both are zero.
But the "reflecting" of the terminal efflux also simulates a theoretical off-limit cell with a theoretical auxin concentration and this influences the PIN distribution.
In a regular closed system, all PINs of the terminal cell would be on the membrane towards the center and, like the "cutoff" condition, repress the generation of a peak near the margins.
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.5]{/1dsimple/data_ss.png}
\caption{Simulation under standard condition with a sink (\textbf{A}) or source (\textbf{B}) in one cell. The location of the depicted cell and the auxin concentration constantly added/removed is described in the title of the plot.}
\label{fig:1dsimple_ss}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=.8]{/1dsimple/Data_ss.png}
\caption{Influence of a auxin source or sink on the model behavior. Plots show the change of minimal auxin concentration (\textbf{A}) and average peak height (\textbf{B}) for relative change of the auxin concentration at position 25 (before the integrational step; see \hyperref{Gitlab}{https://gitlab.com/Fe_Ris/master-files/}).} 
\label{fig:1dsimple_ss2}
\end{figure}

Other than the general structure of the system, a method to simulate an external exchange is to add a source or sink to the model. 
By adding one constant influx for a source or efflux for a sink to one cell \ref{fig:1dsimple_ss}, the position of the peaks will be strongly influenced. 
A peak will be generated at a source location, a local minimum at a sink. 
However, the effect of a source is by far stronger than the effect from a sink. 
The test showed that for increasing negative values, the minimal auxin concentration decreases to 0.2 at -1 \ref{fig:1dsimple_ss2} and the average peak height exponentially rises for an increasing, positive value. 
The reason for this behavior are the limits for the concentration and the underlying mechanics of the model, the polar auxin transport.
Independent of the strength of the efflux by a sink, the concentration should not be able to decrease below zero. 
Since the influx from the adjacent cells and the constant synthesis/degradation process of the cell itself are still active, the concentration has a constant surplus.\\
An increasing positive value, which would describe a source, leads to an extreme rise of the peak height at the source position.
No upper limit is set for the concentration of a cell and relative to the specified auxin base level \ref{tab:1DSimple}, the degradation rate should increase as well, but the peak height reaches unexpected and theoretically impossible values.
The "up-the-gradient" flux from the PAT also results to an increased influx to the source cell, but this wouldn't explain the exorbitant values.
\\

{\large Model Parameter} \\
The model is initiated with the standard initial condition for the testing of the model parameters.
Most of this variables are control parameter that can be used to fine-tune the model and adjust it to experimental or literature information.
\\
\label{1dsimole_A}
The correlation between the initial auxin concentration and the base level $A$ is already shown \ref{fig:1dsimple_iva_aux}, but also without varying the initial value, the final peak height still shows a strong correlation to $A$ (Pearsson correlation: 0.995).
\\ 

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/1dsimple/Data_Ga_v2.png}
\caption{Simulations made under standard conditions with modification of the parameter $G_{a}$ for the auxin synthesis rate. Parameters values range from 0.1 to 2.5.}
\label{fig:1dsimple_Ga}
\end{figure}

The parameter $G_{a}$ affects the rate in the cytosol at which auxin will be generated or degraded towards the base level $A$. 
Increasing it will simultaneously increase the minimal auxin concentration and decrease the peak height \ref{fig:1dsimple_Ga}.
This will finally converge to a point, where the peak height reaches zero and the minimal concentration is equal to the base level.
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/1dsimple/Data_Da_v2.png}
\caption{Simulations made under standard conditions with modification of the parameter $D_{a}$ for the diffusion of auxin between cells. Parameters values range from 0 to 118}
\label{fig:1dsimple_Da}
\end{figure}

Increase of the diffusion coefficient $D_{a}$ will have the similar effect, that peak height decreases until it reaches zero and there is no more pattern observable at steady state \ref{fig:1dsimple_Da}.
But rather than just flatten the pattern, a faster diffusion will repress the ability to generate peaks and therefore, stepwise decrease the overall peak height.
At a certain value, this leads to a decrease of the total peak number and thus, to this rapid change of other test values (i.e. peak height).
\\


\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/1dsimple/Data_Ep_v2.png}
\caption{Simulations made under standard conditions with modification of the parameter $E_{p}$ for the efficiency of PIN proteins to transport PIN. Parameters values range from 0 to 1. While the peak height (red) is nearly zero, fluctuation of the other test values can be ignored.}
\label{fig:1dsimple_Da}
\end{figure}

Since $E_{p}$ is also a control parameter like $G_{a}$ or $D_{a}$, it can be used to modify the rate of the PIN-mediated efflux, but in this model it is defined as the efficiency of the PIN efflux, thus it`s maximal value will be 1.
No PIN efflux produces the same outcome as a high $G_{a}$ or $D_{a}$ and with the standard model parameters, it has to be over 0.8 or 80\% efficiency to allow the formation of a peak \ref{fig:1dsimple_Da}.
\\

\begin{figure}[h!]
\centering
\includegraphics[scale=.6]{/1dsimple/Data_Gp_v2.png}
\caption{Simulations made under standard conditions with modification of the parameter $G_{p}$ for the rate of PIN synthesis. Parameters values range from 0.1 to 10}
\label{fig:1dsimple_Gp}
\end{figure}

Equivalent to $G_{a}$, $G_{p}$ affects the rate of PIN synthesis, but in detail, it affects the amount of PINs that are integrated in one membrane site via exocytosis. 
From the test values \ref{fig:1dsimple_Gp} no significant change is made by modification of $G_{p}$ for the observed range, except at 0.8, where the number of peaks increase with the known side effects.   
\\

\begin{figure}[h!]
\centering
\includegraphics[width=\textwidth]{/1dsimple/Data_m_m4_v2.png}
\caption{Simulations made under standard conditions with modification of the parameter $m$ as a regulation factor for the PIN distribution in a cell. Parameters values range from 0.5 to 5}
\label{fig:1dsimple_m}
\end{figure}

At this point, all control parameters may affect the final state of the simulation in a diverse range. 
This section  individual parameter changes were mainly tested and how they affect the global behavior of the model, but simultaneous variation of two values (i.e.\ref{fig:1dsimple_iva_aux}) may have influence on each other.
Further, the calculated test values also revealed strong correlations between some features of the model. 
Even the size of the simulated model, which is in the one-dimensional case defined by the number of adjacent cells along one axis, affects the behavior \ref{fig:1dsimple_cn}. 
It is clearly observable, that on one hand, that the size of the system alone can affect the pattern, but at certain values (i.e. cell number N=34), a binary-like change of multiple test values (peak distance, peak height, peak number and minimal auxin concentration) happened.
It seems, that, with a linear increase of the system size, peak sizes increase until the concentration of auxin reaches a value, where the peak number increases.
Therefore, leading to an abrupt decrease of the average peak height and, of course, distance between peaks. 
The rate at which the auxin increase at peak locations also depends on the total number of peaks in the simulation.
However, behavior between a cell number of 43 to 44, where the reverse effect happens, supports the conclusion, that the overall behavior of the model can be highly affected by the modification of a single parameter, but the final pattern of the model cannot be easily predicted from the selected parameters.  
Standard values and tested parameter ranges for this and later models are not or at least partly based on experimental evidence and this tests should emphasize the flexibility of the model.


\bibliographystyle{plain}
\bibliography{master-ref2}

\end{document}