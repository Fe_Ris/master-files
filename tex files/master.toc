\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Model structure}{5}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Test values}{6}{subsection.1.2}
\contentsline {section}{\numberline {2}Models}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}One-dimensional auxin pattern model}{7}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Mathematical equations}{9}{subsubsection.2.1.1}
\contentsline {subsection}{\numberline {2.2}Analysis}{11}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Apoplast model}{19}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Mathematical equations}{20}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Analysis}{21}{subsubsection.2.3.2}
\contentsline {subsection}{\numberline {2.4}MoleculeX model}{32}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Mathematical equations}{32}{subsubsection.2.4.1}
\contentsline {subsection}{\numberline {2.5}Analysis}{33}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Simple 2D model}{41}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Other models}{44}{subsection.2.7}
\contentsline {subsubsection}{\numberline {2.7.1}ABP model}{44}{subsubsection.2.7.1}
\contentsline {subsubsection}{\numberline {2.7.2}Leaf margin morphogenesis}{44}{subsubsection.2.7.2}
\contentsline {subsection}{\numberline {2.8}Conclusions}{44}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Outlook}{44}{subsection.2.9}
