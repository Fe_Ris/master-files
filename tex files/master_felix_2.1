\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
%\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{makeidx}
\usepackage{float}
\usepackage{graphicx}
\graphicspath{{../../graphics/}}
\usepackage{wrapfig}
\usepackage{acronym}
\usepackage[onehalfspacing]{setspace}
\usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{hyperref}
\setlength{\parindent}{0pt}
\newcommand{\comment}[1]{}


\author{Felix Risse}
\title{Computational models of pattern generation during developmental processes in plants}
\date{2020}

\comment{
 acronyms
\newglossaryentry{}{}
{TIBA}{2,3,5-trilodobenzoic acid}
{PBC}{periodic boundary condition}
{PAT}{polar auxin transport}
{IAA}{Indole-3-acetic acid}
{ABP}{Auxin binding protein}

{$\Phi$}{Phi}
{$\Theta$}{Theta}
}

%\makeglossaries
\begin{document}

\maketitle
\clearpage

\pagenumbering{roman}
\tableofcontents
\clearpage

\subsection{Model structure}
\label{sec:MS}

\begin{figure}
\centering
\includegraphics[scale=0.7]{1D_border_condition.png}
\caption{Border conditions for one-dimensional model defining the concentration values of auxin and pin in the first cell that would border the terminal cells of the system; (\textbf{A} "cutoff") off-limit values are always zero; (\textbf{B} "PBC") off-limit cells copy values of terminal cell at opposite end of the system; (\textbf{C} "zero-flux") off-limit cells mirror values of terminal cells to set the netflux at the ends of the system to zero}
\label{fig:F_bc}
\end{figure}

Most of the growth-related processes only depend on the auxin distribution or rather accumulation in a single layer of cells. Since the an important feature of this processes is the \hyperref[sec:PAT]{PAT}, the representation in a one-dimensional system is sufficient for purpose of this process. 
The one-dimensional models are based on a semi-closed system, where every cell can only exchange molecules to the right or left adjacent cell, but nowhere else. \\
To simulate a possible effects by the environment, three possible conditions for the off-limit cells were implemented, named "cutoff","PBC" and "zero-flux" (Fig.\ref{fig:F_bc}). \\
The "cutoff" condition simulates "sinks" at the outside of the system. 
The netflux to the environment can only be negative.\\
As the standard border condition, the "PBC" is used. 
It will pass the values of the outmost terminal cell to the opposite side of the system and \textit{vice versa}. 
This condition will let the process of the auxin pattern generation pass the borders of the model instead of limiting it and wouldn't affect the process as it can be seen in the results of the \hyperref[sec:1Dmbt]{model tests}.\\
The third border condition is "zero-flux", which can be defined as the opposite of the "cutoff". 
While the "cutoff" allows auxin to leave the system, the "zero-flux" condition sets the netflux of the terminal cells to the environment to zero. 
The influx from the off-limit cells is equal to the efflux of the terminal cells.
This example would be closer to the definition of a closed system, but it still imitates an influx and will by that influence the pin distribution in the terminal cells.\\

\subsection{Test values}
\label{sec:TV}

\begin{figure}
\includegraphics[scale=0.4]{1dplot_stat.png}
\caption{Visualizations of the calculated test values for simulations of the "Simple 1D model".}
\label{fig:F_tv}
\end{figure}

To analyze and compare the tested models, additional test values for characteristics of individual simulations are calculated.
Results of each test are not stored, but can be aquired by using the jupyter notebook files on \href{https://gitlab.com/Fe_Ris/master-files}{GitLab}.
These values are based on the expected result of a model, which is can be shown by the defined standard conditions.
The optimal outcome for the one-dimensional models would be a regular, periodical pattern in the distribution of auxin over the complete system.\\
All simulations should reach an steady state, where calculated values won't change by further integration, but time until it is reached highly depends on the selected model and parameter set.
The other values (see \ref{fig:F_tv}) are calculated, when the steady state is already reached and concentrate on the concentration peaks and the regularity of their distribution.\\
"No. auxin peaks" shows the number of how often the selected model could generate auxin peaks for the specified system size, independent of the boundary condition.
A cell has a peak, if the auxin concentration is higher than in both of the adjacent cells.\\  
The "avg. peak height" is the average difference between the lowest global auxin concentration and the peak concentrations.
The width of a peak "avg. peak width" is defined by the number of cells near a peak with over half the concentration of the averaged peak height.
The last test value is the middle distance in cells between identified peaks.
For the purpose of a theoretically continuous system, if we use the \ref{fig:F_bc}{"periodic boundary condition"}, the distance between the first and last peak "over the boundaries" will also be considered, even if the "PBC" is not selected.  
 

\section{Models}

The following one-dimensional models are based on the work of \textit{Fujita et. al} \cite{Fujita2018}and the term "one-dimensional" refers to the arrangement of the cells in these models. 
They intended to give an insight into self-organizing, regular patterns in multicellular organism (i.e. phyllotaxis). 

\subsection{One-dimensional auxin pattern model}
\label{sec:1Dmodel}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{master_auxin_1Donecell.png}
\caption{Sketch of a single cell in the simple one-dimensional model; each cell has two defined sites on the cell membrane with a variable amount of PIN proteins; auxin is synthesized in the cell's cytosol and passes the membrane actively via PINs or passively via diffusion}
\end{figure}

In order to simulate the regular pattern generation of auxin along multiple cells, driven by the \hyperref[sec:PAT]{PAT}, a model system with a one-dimensional cell layer, PIN proteins and Auxin is used. 
The whole model is build from a row of square cells, which all are based on the same cell type and only differ by the magnitude of their features (i.e. cytosolic auxin concentration). 
There are two possible conditions for the border: a \hyperref[sec:PBC]{periodic boundary} or a zero netflux, which you can imagine as a neighboring cell with exactly the same efflux to it's side as the outer cell. \\
The simple model just consists of two components, auxin and PIN proteins. 
The process of generating a regular pattern in terms of the distribution of auxin along the cells is mainly based on the interaction of these two molecules and their mutual feedback regulation. 
Auxin can either diffuse from one to a neighboring cell or could be actively transported by PIN proteins in the plasmamembrane. 
Although, according to the \hyperref[sec:CM]{Chemiosmotic model}, auxin should not be able to simply diffuse out of a cell, a possible diffusion is assumed, since the cells in this model have no extracellular space in between and the difference of the pH of environments/compartments is not included as an influencal factor.
On the other hand, PIN proteins are not fixed in their location. 
They are in a constant re-cycling process and transported via vesicle transport between the membrane and vacuole, where they are assumed to degenerate and synthesize. 
This PIN cycling makes the distribution of PIN on the membrane very dynamic and is, therefore, a key to this feedback process, because the location where PINs are re-integrated into the membrane is dependent on the relative concentration of auxin of the neighboring cell in that direction. 
In case of this one-dimensional model with two neighbors per cell, the amount of PINs will be consequently higher on the membrane side, where the other cell has a higher auxin concentration. 
As an effect, more auxin will be actively transported to this side and increase the amount in the neighboring cell even more. 
This netflux can be described as "up-the-gradient", because normally, the gradient of two different concentrations would point to the lower one, leading to the equalization of the conentrations, but the principle of the PIN-mediated auxin flux works exactly in the opposite direction. \\
Like PINs, auxin is synthesized and degraded in each cell. 
Without flux or diffusion of auxin, there will be an equilibrium between the rate of synthesis and degration, which is used in the model as an "base" concentration of auxin. 
This also inhibits the ongoing increase of auxin in some cells by the PIN-mediated flux and at some point, there will be an equilibrium between the increase by flux and decrease towards the base concentration. \\

\begin{figure}
\includegraphics[width=1\textwidth]{1dplot.png}
\caption{One-dimensional auxin pattern model at steady state; auxin concentration of the cells in the model are described by the (blue) bars. Initial distribution of auxin with a random noise of 1\% marked by the (red) dashed line. The total amount of auxin in one period (peak-to-peak) is nearly equal to the initial amount.}   
\end{figure}

\subsubsection{Mathematical equations}
\label{sec:1Dequation}

The mathematical equations for the model come from \textit{Fujita et. al} \cite{Fujita2018}. 
Since the location of a component can be described by a discrete parameter, the simple one-dimensional model could defined by 3 ordinary differential equations, independent of the total size of the simulated system. 
The time for one simulation, although, can increase exponentially for the number of cells in the system. \\

\begin{equation}
\large \frac{da_{i}}{dt}=G_{a}(A-a_{i})-\sum_{j} f_{i,j} +\sum_{j} D_{a}(a_{j}-a_{i}) \label{eq:1}
\end{equation}
\addtocounter{equation}{-1}

For the change of the concentration change of auxin of a cell ($ \frac{dta_{i}}{dt} $), the equation can be divided into three parts by their function: synthesis, PIN-mediated flux and diffusion. 

\begin{subequations}
\begin{align}
\large G_{a}(A-a_{i}) \label{eq:1a}
\end{align}

Each cell has a synthesis and degradation rate of auxin, which are more or less dependent of the actual concentration of auxin inside the cytosol. 
A higher concentration would shift the equilibrium of this rates to the degradation and \textit{vice versa}. 
$A$ is this base concentration or equilibrium constant that determines, if the cell mainly generate or degrades auxin. 
$G_{a}$ is the control parameter for this term.


\begin{align}
\large -\sum_{j} f_{i,j} \quad with \quad f_{i,j}=E_{p}(p_{i,j}a_{i}-p_{j,i}a_{j}) \label{eq:1b}
\end{align}


Both sides of the cell's plasmamembrane inhabit an amount of internal PIN proteins, which actively transport auxin to the neighboring cell. 
The efflux on one side depends on the PIN concentration $p_{i,j}$ on the membrane from the own cell $i$ to the next cell $j$ and auxin $a_{i}$. 
The influx is simply the efflux of the neighbor cell $p_{j,i}a_{j}$ to cell $i$. 
Again, $E_{p}$ is a control parameter for this term ,showing the efficiency of the PIN-mediated transport.

\begin{align}
\sum_{j} D_{a}(a_{j}-a_{i}) \label{eq:1c}
\end{align}
\end{subequations}

The last part of the equation is for the change by diffusion, which basically is a concentration gradient to the subsequent cell for both sides $a_{j}-a_{i}$ and a control parameter $D_{a}$. \\
PINs are described by one equation per membrane side with a neighboring cell with only a term that resembles the synthesis part of the auxin equation.

\begin{equation}
\large \frac{p_{i,j}}{dt}=G_{p}\left(Kp\frac{\Phi(a_{j})}{\sum_{j}\Phi(a_{j})}-p_{i,j}\right) \label{eq:2}
\end{equation}

$p_{i,j}$ is the membrane's PIN concentration, $K$ the number of neighbors and $G_{p}$ a control parameter. 
Instead of just a base concentration, here as $p$, the relative excess of auxin in the subsequent cell compared to every neighboring cell ($\frac{\Phi(a_{j})}{\sum_{j}\Phi(a_{j})}$ is also calculated. 
The regulatory function $\Phi()$ can be used to modify the type and strength of the influence of auxin to PIN. 
For this model, this function is $\Phi(a_{j})=a_{j}^{m}$ with m as an arbitrary parameter. \\

\subsection{Model behavior tests}
\label{sec:1Dmbt}

A computational model of a biological process or system is just a simplification and should be able to behave at least in some features like its origin in nature. For the one-dimensional model, this would be the outer layer (L1) of an shoot apical meristem (SAM) in the growth phase of a higher plant \cite{adamowski2015}. 
In this section, the behavior of the model was tested for different variations of it's initial conditions (i.e. cell number, initial values, etc.) and parameters. 
In each test, at least one variable is modified with every other value kept at a the pre-defined standard.
This standards are not based on experimental results or lituerature due to the lack of reference, but lead to a representative solution.
Altough the mathematical equations of these models may be from other publications, the implementation into a computational model is completely my own work. \\
The same testing procedure is individually adjusted to be able for the usage of the other models, so that the results of all test simulations are comparable.
Detailed information about the code of implemented models and additionally used functions can be found on \href{https://gitlab.com/Fe_Ris/master-files}{GitLab}.
 
 
\begin{table}
\centering
\begin{tabular}{|r|r|r|}
\hline
Variable&Value	&Definition\\
\hline
\multicolumn{3}{|c|}{Conditions}\\
\hline
N		&50		&total cell number\\
K		&2		&number of adjacent cells per cell\\
Auxin	&5		&initial auxin concentration per cell\\
PIN		&40		&initial PIN concentration per membrane side per cell\\
Time	&100000	&maximal integration time\\
Noise	&0.01	&relative noise on initial values\\
PBC		&1		&boundary condition\\
\hline
\multicolumn{3}{|c|}{Parameter}\\
\hline
$G_{a}$	&0.01	&auxin synthesis coefficient\\
A		&5		&auxin base level\\
$D_{a}$	&100	&auxin diffusion coefficient\\
$G_{p}$	&1		&PIN synthesis coefficient\\
P		&40		&PIN base level\\
m		&2		&PIN regulation factor\\
$E_{p}$	&1		&PIN efflux efficiency coefficient\\
\hline
\end{tabular}
\caption{Values and definitions of the parameter used as the standard for behavior tests of the Simple 1D model.}
\label{tab:1DSimple}
\end{table}


The initial conditions of the "Simple 1D model" are the size of the simulated model system (N) and distribution of the two components Auxin and PIN over the whole system.
Since these models are only one-dimensional, each cell can have at most two adjacent neighbor cells (K). 
Other initial parameters, like the boundary condition (PBC) \ref{sec:MS} and noise, which adds distortion to the initial distributions, are modifications of this conditions.\\
There are seven model parameters in this model \ref{sec:1Dequation}, which will be inherited by following models that are based on \cite{Fujita2018}.\\
\\

\comment{
\begin{figure}
\centering
\includegraphics[scale=1]{/1dsimple/.png}
\caption{}
\label{}
\end{figure}
}

\begin{figure}[H]
\centering
\includegraphics[scale=.7]{/1dsimple/Data_cn_5_100.png}
\caption{Test results of the 1D Simple model under standard conditions with varying size from 5 to 100 cells. Value of the tested variable is plotted against the test values. The left axis is for the steady state and the other for the remaining test values with their unit depicted in the legend.}
\label{fig:1dsimple_cn}
\end{figure}

%66/36/28
By varying the size of the system, it is shown, that under the standard condition, the model does need at least a minimal number of cells to develop it's typical "wave pattern".
This also highly depends on the type of boundary condition \ref{fig:F_bc}.
While a closed system ("zero-flux") and a system, which is theoretically expanded over it's original size ("PBC") show the first occurrence of a second peak by relativly small cell number with 28 and 36 cells, respectively,
an open system ("cutoff") needs to be double that size. 
Clearly observable is the correlation between the peak height, which includes the minimal auxin concentration, and the peak distance \ref{fig:1dsimple_cn}. They show a linear increase, but the number of peaks act like a switch, that determines the slope of the rate at which the peaks grow.
\\

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{/1dsimple/Data_iva_aux_switch*.png}
\caption{Simulations under standard conditions with variation of the initial auxin value. Steady state pattern of auxin with 20 (\textbf{A}) and 25 (\textbf{B}) $\mu mol/\mu m^{3}$. (\textbf{C}) Test results for increasing initial auxin concentration and the base value concentration $A$ set to a fifth of it. }
\label{fig:1dsimple_iva_aux}
\end{figure}

The inital values for auxin and PIN doesn't show an effect on outcome of the simulation, except for auxin, when it is at least five-fold of the base level concentration \ref{fig:1dsimple_iva_aux}. 
If the initial concentration is below this threshold, the resulting pattern is as expected, but if it is higher, it just generates a single, significantly higher, peak. 
Tests over a range of initial concentrations, where the base is always set to a fifth, the peak height is linearly increasing, while other values are not changing at all.
This correlation could not be evaluated for PIN and the parameter $p$, which defines the base level for integrated PIN proteins at one membrane site.
\\

\begin{figure}
\centering
\includegraphics[scale=.5]{/1dsimple/Data_noi_0.png}
\caption{Simulation with standard conditions and no noise.}
\label{fig:1dsimple_noi}
\end{figure}

To initiate the process, there need to be some inequality in the distribution of auxin or PIN, since the \comment{\ref{section PAT}{polar auxin transport}} needs a gradient of the auxin concentration between adjacent cells to happen.
Thus, without noise, the system keeps the even auxin distribution and instantly enters it steady stage \ref{fig:1dsimple_noi}.
Other than leading to the initiation of the process, noise has no further effect on the system.
\\

\begin{figure}[H]
\centering
\includegraphics[scale=.4]{/1dsimple/data_bc.png}
\caption{Simulation with standard conditions and the three possible structure modes for the system: (\textbf{A}) an open system, where auxin can leave to the environment, (\textbf{B}) a closed, continuous system and (\textbf{C}) a semi-closed system.}
\label{fig:1dsimple_bc}
\end{figure}

One important factor for the general behavior of the model is the definition of the system boundaries \ref{sec:MS}.
Either if it is in contact with the environment (\ref{fig:1dsimple_bc},"cutoff") or not ("PBC" or "zero-flux").
The "cutoff" condition increases the efflux of the terminal cells and inhibits the generation of peaks near the margins of the system, if it's size is too small.\\
For the one-dimensional model, the system with "PBC" could be imagined as a closed circle of cells.
If the system is sufficiently big, the terminal cells only interact with each other over the border and as a factor for the pattern generation, the system seems broader than it really is.\\
The "zero-flux" condition also leads to a more different result than a regular closed system.
In the case of auxin concentrations, there is no difference, if the influx is equal to the efflux or both are zero.
But the "reflecting" of the terminal efflux also simulates a theoretical off-limit cell with a theoretical auxin concentration and this influences the PIN distribution.
In a regular closed system, all PINs of the terminal cell would be on the membrane towards the center and, like the "cutoff" condition, repress the generation of a peak near the margins.
\\

\begin{figure}[H]
\centering
\includegraphics[scale=.5]{/1dsimple/data_ss.png}
\caption{Simulation under standard condition with a sink (\textbf{A}) or source (\textbf{B}) in one cell. The location of the depicted cell and the auxin concentration constantly added is described in the title of the plot.}
\label{fig:1dsimple_ss}
\end{figure}

Other than the general structure of the system, a method to simulate an external exchange is to add a source or sink to the model. 
By adding one constant influx for a source or efflux for a sink to one cell \ref{fig:1dsimple_ss}, the position of the peaks can be defined before the model is initiated. 
A peak will be generated at a source location, a local minimum at a sink.\\
The combination of two Source/Sink locations can, depended on their distance, markedly change the outcome, but the pattern is still detectable. 
For example, if two sources are relatively close, there will be a peak in between, but otherwise there is a local minimum.
\\

\large Model Parameter 
The model is initiated with the standard initial condition for the testing of the model parameters.
Most of this variables are control parameter that can be used to fine-tune the model and adjust it to experimental or literature information.
\\
The correlation between the initial auxin concentration and the base level $A$ is already shown \ref{fig:1dsimple_iva_aux}, but also without varying the initial value, the final peak height still shows a strong correlation to $A$ (Pearsson correlation: 0.995).
\\ 

\begin{figure}[H]
\centering
\includegraphics[scale=.6]{/1dsimple/Data_Ga.png}
\caption{Simulations made under standard conditions with modification of the parameter $G_{a}$ for the auxin synthesis rate. Parameters values range from 0.1 to 2.5.}
\label{fig:1dsimple_Ga}
\end{figure}

The parameter $G_{a}$ affects the rate in the cytosol at which auxin will be generated or degraded towards the base level $A$. 
Increasing it will simultaneously increase the minimal auxin concentration and decrease the peak height \ref{fig:1dsimple_Ga}.
This will finally converge to a point, where the peak height reaches zero and the minimal concentration is equal to the base level.
\\

\begin{figure}[H]
\centering
\includegraphics[scale=.5]{/1dsimple/Data_Da.png}
\caption{Simulations made under standard conditions with modification of the parameter $D_{a}$ for the diffusion of auxin between cells. Parameters values range from 0 to 118}
\label{fig:1dsimple_Da}
\end{figure}

Increase of the diffusion coefficient $D_{a}$ will have the similar effect, that peak height decreases until it reaches zero and there is no more pattern observable at steady state \ref{fig:1dsimple_Da}.
But rather than just flatten the pattern, a faster diffusion will repress the ability to generate peaks and therefore, stepwise decrease the overall peak height and at a certain value, decrease the total number of peaks.
\\


\begin{figure}
\centering
\includegraphics[scale=.5]{/1dsimple/Data_Ep.png}
\caption{Simulations made under standard conditions with modification of the parameter $E_{p}$ for the efficiency of PIN proteins to transport PIN. Parameters values range from 0 to 1. While the peak height (red) is nearly zero, fluctuation of the other test values can be ignored.}
\label{fig:1dsimple_Da}
\end{figure}

Since $E_{p}$ is also a control parameter like $G_{a}$ or $D_{a}$, it can be used to modify the rate of the PIN-mediated efflux, but in this model it is defined as the efficiency of the PIN efflux, thus it`s maximal value will be 1.
No PIN efflux produces the same outcome as a high $G_{a}$ or $D_{a}$ and with the standard model parameters, it has to be over 0.8 or 80\% efficiency to allow the formation of a peak \ref{fig:1dsimple_Da}.
\\

\begin{figure}[H]
\centering
\includegraphics[scale=.5]{/1dsimple/Data_Gp.png}
\caption{Simulations made under standard conditions with modification of the parameter $G_{p}$ for the rate of PIN synthesis. Parameters values range from 0.1 to 10}
\label{fig:1dsimple_Gp}
\end{figure}

Equivalent to $G_{a}$, $G_{p}$ affects the rate of PIN synthesis, but in detail, it affects the amount of PINs that are integrated in one membrane site via exocytosis. 
From the test values \ref{fig:1dsimple_Gp} no significant change is made by modification of $G_{p}$ for the observed range, except at 0.8, where the number of peaks increase with the known side effects.   
\\

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{/1dsimple/Data_m_m4.png}
\caption{Simulations made under standard conditions with modification of the parameter $m$ as a regulation factor for the PIN distribution in a cell. Parameters values range from 0.5 to 5}
\label{fig:1dsimple_m}
\end{figure}


\bibliographystyle{plain}
\bibliography{master-ref}

\end{document}