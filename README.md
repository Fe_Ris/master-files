# Master files

Jupyter notebooks for my master thesis.

Files are written with Jupyter notebook.

Used:  
python 3.7.3  
jupyter-notebook 4.4.0  
Assimulo 3.0  
k3d-jupyter 2.7.2  

Each model has three parts: a class, helper functions and the testing script.  
The Class file (Classes.ipynb) contains the class for each model.
The Helper functions file (Helper_functions.ipynb) contains all additional functions outside of the classes.
The testing scripts are specified for each model and can be used to test the flexibility and behavior of it.

Detailed explanaition may be found in the wiki.

